package com.example.demo.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "groups")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_code", unique = true, nullable = false)
    private Long groupCode;

    @Column(name = "specialty_code")
    private Long specialtyCode;

    @Column(name = "specialty_name")
    private String specialtyName;

    @Column(name = "students_number")
    private Integer studentsNumber;

    public Group() {
    }

    public Group(Long groupCode, Long specialtyCode, String specialtyName) {
        this.groupCode = groupCode;
        this.specialtyCode = specialtyCode;
        this.specialtyName = specialtyName;
    }

    public Group(Long groupCode, Long specialtyCode, String specialtyName, Integer studentsNumber) {

        this.groupCode = groupCode;
        this.specialtyCode = specialtyCode;
        this.specialtyName = specialtyName;
        this.studentsNumber = studentsNumber;
    }

    public Long getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(Long groupCode) {
        this.groupCode = groupCode;
    }

    public Long getSpecialtyCode() {
        return specialtyCode;
    }

    public void setSpecialtyCode(Long specialtyCode) {
        this.specialtyCode = specialtyCode;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public Integer getStudentsNumber() {
        return studentsNumber;
    }

    public void setStudentsNumber(Integer studentsNumber) {
        this.studentsNumber = studentsNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return Objects.equals(groupCode, group.groupCode) &&
                Objects.equals(specialtyCode, group.specialtyCode) &&
                Objects.equals(specialtyName, group.specialtyName) &&
                Objects.equals(studentsNumber, group.studentsNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupCode, specialtyCode, specialtyName, studentsNumber);
    }

    @Override
    public String toString() {
        return "Group{groupCode=" + groupCode +
                ", specialtyCode=" + specialtyCode +
                ", specialtyName='" + specialtyName + '\'' +
                ", studentsNumber=" + studentsNumber +
                '}';
    }
}
