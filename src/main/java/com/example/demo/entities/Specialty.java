package com.example.demo.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "specialties")
public class Specialty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specialty_code", unique = true, nullable = false)
    private Long specialtyCode;

    @Column(name = "specialty_name", unique = true, nullable = false)
    private String specialtyName;

    public Specialty() {
    }

    public Specialty(Long specialtyCode, String specialtyName) {
        this.specialtyCode = specialtyCode;
        this.specialtyName = specialtyName;
    }

    public Long getSpecialtyCode() {
        return specialtyCode;
    }

    public void setSpecialtyCode(Long specialtyCode) {
        this.specialtyCode = specialtyCode;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Specialty)) return false;
        Specialty specialty = (Specialty) o;
        return Objects.equals(specialtyCode, specialty.specialtyCode) &&
                Objects.equals(specialtyName, specialty.specialtyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(specialtyCode, specialtyName);
    }

    @Override
    public String toString() {
        return "Specialty{specialtyCode=" + specialtyCode +
                ", specialtyName=" + specialtyName +
                '}';
    }
}
