package com.example.demo.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "entrance_tests")
public class EntranceTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "list_number", unique = true, nullable = false)
    private Long listNumber;

    @Column(name = "student_code", unique = true, nullable = false)
    private Integer studentCode;


    @Column(name = "assessment")
    private Integer assessment;

    public EntranceTest() {
    }

    public EntranceTest(Long listNumber, Integer studentCode) {
        this.listNumber = listNumber;
        this.studentCode = studentCode;
    }

    public Long getListNumber() {
        return listNumber;
    }

    public void setListNumber(Long listNumber) {
        this.listNumber = listNumber;
    }

    public Integer getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(Integer studentCode) {
        this.studentCode = studentCode;
    }

    public Integer getAssessment() {
        return assessment;
    }

    public void setAssessment(Integer assessment) {
        this.assessment = assessment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EntranceTest)) return false;
        EntranceTest that = (EntranceTest) o;
        return Objects.equals(listNumber, that.listNumber) &&
                Objects.equals(studentCode, that.studentCode) &&
                Objects.equals(assessment, that.assessment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(listNumber, studentCode, assessment);
    }

    @Override
    public String toString() {
        return "EntranceTest{listNumber=" + listNumber +
                ", studentCode=" + studentCode +
                ", assessment=" + assessment +
                '}';
    }
}
