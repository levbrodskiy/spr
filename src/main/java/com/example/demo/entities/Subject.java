package com.example.demo.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "subjects")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_code", unique = true, nullable = false)
    private Long subjectCode;

    @Column(name = "subject_name", unique = true, nullable = false)
    private String subjectName;

    public Subject() {
    }

    public Subject(Long subjectCode, String subjectName) {
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
    }


    public Long getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(Long subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subject)) return false;
        Subject subject = (Subject) o;
        return Objects.equals(subjectCode, subject.subjectCode) &&
                Objects.equals(subjectName, subject.subjectName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjectCode, subjectName);
    }

    @Override
    public String toString() {
        return "Subject{ subjectCode=" + subjectCode +
                ", subjectName='" + subjectName + '\'' +
                '}';
    }
}
