package com.example.demo.services;

import com.example.demo.dao.EntranceTestDao;
import com.example.demo.entities.EntranceTest;

import java.util.List;

public class EntranceTestService {
    private final EntranceTestDao entranceTestDao = new EntranceTestDao();

    public List<EntranceTest> getAllEntranceTests() {
        return entranceTestDao.findAll();
    }

    public EntranceTest getBySpecialtyCode(long listNumber){
        return entranceTestDao.findByListNumber(listNumber);
    }

    public void saveEntranceTest(EntranceTest entranceTest){
        entranceTestDao.save(entranceTest);
    }

    public void updateEntranceTest(EntranceTest entranceTest) throws NullPointerException {
        entranceTestDao.update(entranceTest);
    }

    public void deleteEntranceTest(EntranceTest entranceTest){
        entranceTestDao.delete(entranceTest);
    }
}
