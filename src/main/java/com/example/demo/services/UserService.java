package com.example.demo.services;

import com.example.demo.dao.UserDao;
import com.example.demo.entities.User;


import java.util.List;

public class UserService {
    private final UserDao userDao = new UserDao();

    public boolean register(User user){
        user.setId(null);


        if (userDao.findByName(user.getName()) == null){
            userDao.save(user);
            return true;
        }

        return false;


    }

    public boolean authorize(User user){
        user.setId(null);


        if (userDao.findByName(user.getName()) == null){
            return false;
        }

        return true;

    }
    public void delete(User user){
        userDao.delete(user);
    }

    public void update(User user){
        userDao.update(user);
    }

    public List<User> findAll(){
        return userDao.findAll();
    }
}
