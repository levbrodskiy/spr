package com.example.demo.services;

import com.example.demo.dao.SubjectDao;
import com.example.demo.entities.Subject;

import java.util.List;

public class SubjectService {
    private final SubjectDao subjectDao = new SubjectDao();

    public List<Subject> getAllSubjects() {
        return subjectDao.findAll();
    }

    public Subject getBySubjectCode(long subjectCode){
        return subjectDao.findBySubjectCode(subjectCode);
    }

    public void saveSubject(Subject subject){
        subjectDao.save(subject);
    }

    public void updateSubject(Subject subject) {
        subjectDao.update(subject);
    }

    public void deleteSubject(Subject subject){
        subjectDao.delete(subject);
    }
}
