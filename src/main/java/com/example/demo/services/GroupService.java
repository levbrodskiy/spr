package com.example.demo.services;

import com.example.demo.dao.GroupDao;
import com.example.demo.entities.Group;

import java.util.List;

public class GroupService {
    private final GroupDao groupDao = new GroupDao();

    public List<Group> getAllGroup() {
        return groupDao.findAll();
    }

    public Group getByGroupCode(long specialtyCode){
        return groupDao.findByGroupCode(specialtyCode);
    }

    public void saveGroup(Group group){
        groupDao.save(group);
    }

    public boolean updateGroup(Group group) {
        if (true){
            groupDao.update(group);
        }

        return false;
    }

    public void deleteGroup(Group group){
        groupDao.delete(group);
    }
}
