package com.example.demo.services;

import com.example.demo.dao.SpecialtyDao;
import com.example.demo.entities.Specialty;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecialtyService {
    private final SpecialtyDao specialtyDao = new SpecialtyDao();

    public List<Specialty> getAllSpecialties() {
        return specialtyDao.findAll();
    }

    public Specialty getBySpecialtyCode(long specialtyCode){
        return specialtyDao.findBySpecialtyCode(specialtyCode);
    }

    public void saveSpecialty(Specialty specialty){
        specialtyDao.save(specialty);
    }

    public void updateSpecialty(Specialty specialty) throws NullPointerException {
        specialtyDao.update(specialty);
    }

    public void deleteSpecialty(Specialty specialty){
        specialtyDao.delete(specialty);
    }

}
