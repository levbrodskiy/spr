package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

@org.springframework.boot.autoconfigure.SpringBootApplication
public class SpringBootApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SpringBootApplication.class, args);
		//Specialty specialty = new Specialty(null, "87dhg5df7564");
		//SpecialtyDao specialtyDao = new SpecialtyDao();
		//specialtyDao.save(specialty);
	}

}
