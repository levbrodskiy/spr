package com.example.demo.controllers;

import com.example.demo.entities.Specialty;
import com.example.demo.services.SpecialtyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

@Controller
@RequestMapping("/specialties")
public class SpecialtiesController {
    SpecialtyService specialtyService = new SpecialtyService();

    @GetMapping
    public String getAll(Model model) {
        List<Specialty> specialties = specialtyService.getAllSpecialties();
        model.addAttribute("specialties", specialties);
        return "specialties";
    }

    @PostMapping("/create")
    public String create(
            @RequestParam(name = "subject_code") long specialtyCode,
            @RequestParam(name = "subject_name") String specialtyName,
            Model model){

        specialtyService.saveSpecialty(new Specialty(specialtyCode, specialtyName));

        return "redirect:home";
    }

    @PostMapping("/update")
    public String update(
            @RequestParam(name = "subject_code") long specialtyCode,
            @RequestParam(name = "subject_name") String specialtyName,
            Model model){

        specialtyService.saveSpecialty(new Specialty(specialtyCode, specialtyName));

        return "redirect:home";
    }

    @PostMapping("/delete")
    public String delete(
            @RequestParam(name = "subject_code") long specialtyCode,
            @RequestParam(name = "subject_name") String specialtyName,
            Model model){

        specialtyService.saveSpecialty(new Specialty(specialtyCode, specialtyName));

        return "redirect:home";
    }
}