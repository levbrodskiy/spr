package com.example.demo.controllers;

import com.example.demo.entities.Subject;
import com.example.demo.services.SubjectService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

@Controller
@RequestMapping("/subjects")
public class SubjectsController {
    SubjectService subjectService = new SubjectService();

    @GetMapping
    public String getAll(Model model) {
        List<Subject> subjects = subjectService.getAllSubjects();
        model.addAttribute("subjects", subjects);
        return "subjects";
    }

    @PostMapping("/create")
    public String create(
            @RequestParam(name = "subject_code") long subjectCode,
            @RequestParam(name = "subject_name") String subjectName,
            Model model){

        subjectService.saveSubject(new Subject(subjectCode, subjectName));

        return "redirect:home";
    }

    @PostMapping("/update")
    public String update(
            @RequestParam(name = "subject_code") long subjectCode,
            @RequestParam(name = "subject_name") String subjectName,
            Model model){

        subjectService.updateSubject(new Subject(subjectCode, subjectName));

        return "redirect:home";
    }

    @PostMapping("/delete")
    public String delete(
            @RequestParam(name = "subject_code") long subjectCode,
            @RequestParam(name = "subject_name") String subjectName,
            Model model){

        subjectService.deleteSubject(new Subject(subjectCode, subjectName));

        return "redirect:home";
    }
}
