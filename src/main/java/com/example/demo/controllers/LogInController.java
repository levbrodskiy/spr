package com.example.demo.controllers;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LogInController {
    UserService userService = new UserService();
    @GetMapping("/registration")
    public String getLogin() {
        return "registration";
    }

    @PostMapping("/registration")
    public String postLogin(@RequestParam(defaultValue = "name") String name,
                            @RequestParam(defaultValue = "key") String key) {
        if (userService.register(new User(null, name, key))){
           return "redirect:/";
        }
        System.out.println(name + " " + key);
        return "redirect:/authorization";
    }
}
