package com.example.demo.controllers;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/authorization")
public class SingInController {
    UserService userService = new UserService();

    @GetMapping()
    public String getAuthorization() {
        return "singin";
    }

    @PostMapping
    public String postAuthorization(@RequestParam(name = "name") String name,
                            @RequestParam(name = "key") String key) {
        if (userService.authorize(new User(null, name, key))){
            return "redirect:/";
        }

        return "singin";
    }
}
