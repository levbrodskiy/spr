package com.example.demo.controllers;

import com.example.demo.entities.Group;
import com.example.demo.services.GroupService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/groups")
public class GroupsController {
    GroupService groupService = new GroupService();

    @GetMapping
    public String getGroups(Model model){
        return "";
    }

    @PostMapping("/edit")
    public String editGroup(@RequestParam(name = "group_code")Long groupCode,
                            @RequestParam(name = "specialty_code")Long specialtyCode,
                            @RequestParam(name = "specialty_name")String specialtyName){

        if (groupService.updateGroup(new Group(groupCode, specialtyCode, specialtyName))){

        }


        return null;
    }

}
