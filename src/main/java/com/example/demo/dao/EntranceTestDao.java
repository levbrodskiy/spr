package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.EntranceTest;
import com.example.demo.entities.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class EntranceTestDao {
    public EntranceTest findByListNumber(long listNumber) {
        return HibernateUtils.getSessionFactory().openSession().get(EntranceTest.class, listNumber);
    }

    public boolean save(EntranceTest entranceTest) {
        try (Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            if (entranceTest.getStudentCode() == null){
                return false;
            }
            Student student = session.get(Student.class, entranceTest.getStudentCode());
            if (student == null){
                return false;
            }
            entranceTest.setListNumber(null);
            session.save(entranceTest);
            tx1.commit();
            session.close();
            return true;
        }
    }

    public boolean update(EntranceTest entranceTest) {
        try (Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            if (entranceTest == null){
                return false;
            }

            EntranceTest test = session.get(EntranceTest.class, entranceTest.getListNumber());

            if (test == null){
                return false;
            }

            if (entranceTest.getStudentCode() == null){
                return false;
            }

            Student student = session.get(Student.class, entranceTest.getStudentCode());

            if (student == null){
                return false;
            }

            session.update(entranceTest);
            tx1.commit();
            session.close();
            return true;
        }
    }

    public void delete(EntranceTest entranceTest) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(entranceTest);
        tx1.commit();
        session.close();
    }

    public List<EntranceTest> findAll() {
        List<EntranceTest> entranceTests = (List<EntranceTest>)  HibernateUtils.getSessionFactory().openSession().createQuery("From Group").list();
        return entranceTests;
    }
}
