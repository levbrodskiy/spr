package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.Group;
import com.example.demo.entities.Specialty;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

@SuppressWarnings("all")
public class SpecialtyDao {
    public Specialty findBySpecialtyCode(Long specialtyCode) {
        return HibernateUtils.getSessionFactory().openSession().get(Specialty.class, specialtyCode);
    }

    public void save(Specialty specialty) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(specialty);
        tx1.commit();
        session.close();
    }

    public void update(Specialty specialty) {
        try(Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            session.update(specialty);

            List<Group> groups = session.createQuery(
                    "FROM Group WHERE specialty_code = "
                            + specialty.getSpecialtyCode()).list();

            if (groups != null){
                for (Group group : groups){
                    group.setSpecialtyCode(specialty.getSpecialtyCode());
                    group.setSpecialtyName(specialty.getSpecialtyName());
                    session.update(group);
                }
            }

            tx1.commit();
            session.close();
        }catch (Throwable th){
            throw new RuntimeException("sql error");
        }
    }

    public void delete(Specialty specialty) {
        try(Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            session.delete(specialty);
            List<Group> groups = session.createQuery(
                    "FROM Group WHERE specialty_code = "
                            + specialty.getSpecialtyCode()).list();

            if (groups != null){
                for (Group group : groups){
                    group.setSpecialtyCode(null);
                    group.setSpecialtyName(null);
                    session.update(group);
                }
            }

            tx1.commit();
            session.close();
        }catch (Throwable th){
            throw new RuntimeException("sql error");
        }
    }

    public List<Specialty> findAll() {
        List<Specialty> specialties =
                (List<Specialty>)HibernateUtils.getSessionFactory()
                        .openSession().createQuery("From Specialty").list();
        return specialties;
    }
}
