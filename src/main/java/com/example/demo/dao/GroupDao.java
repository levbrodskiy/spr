package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.Group;
import com.example.demo.entities.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;

@SuppressWarnings("all")
public class GroupDao {

    public Group findByGroupCode(long groupCode) {
        return HibernateUtils.getSessionFactory().openSession().get(Group.class, groupCode);
    }

    public void save(Group group) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(group);
        tx1.commit();
        session.close();
    }

    public void update(Group group) {
        try(Session session = HibernateUtils.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.update(group);
            tx1.commit();
            session.close();
        }
    }

    public void delete(Group group) {
        try (Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            if (group.getGroupCode() != null) {
                List<Student> students = session.createQuery(
                        "FROM Student WHERE group_code = "
                                + group.getGroupCode()).list();
                if (students == null && students.size() == 0) {
                    for (Student student : students) {
                        student.setGroupCode(null);
                        session.update(student);
                    }
                }
            }
            session.delete(group);
            tx1.commit();
            session.close();
        }
    }

    public List<Group> createQuery(String query){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        Query groupQuery = session.createQuery(query);
        List<Group> groups = groupQuery.list();
        tx1.commit();
        session.close();
        return groups;
    }

    public List<Group> findAll() {
        List<Group> groups = (List<Group>)  HibernateUtils.getSessionFactory().openSession().createQuery("From Group").list();
        return groups;
    }
}
