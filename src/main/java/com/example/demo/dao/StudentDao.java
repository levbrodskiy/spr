package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.EntranceTest;
import com.example.demo.entities.Group;
import com.example.demo.entities.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

@SuppressWarnings("all")
public class StudentDao {
    public Student findById(int id) {
        return HibernateUtils.getSessionFactory().openSession().get(Student.class, id);
    }

    public void save(Student student) {
        try(Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();

            if (student.getGroupCode() != null){
                List<Group> groups = session.createQuery(
                        "FROM Group WHERE group_code = "
                                + student.getGroupCode()).list();
                if(groups == null || groups.size() == 0){
                    student.setGroupCode(null);
                }else {
                    Group group = groups.get(0);
                    group.setStudentsNumber(group.getStudentsNumber() + 1);
                    session.save(group);
                }
            }

            session.save(student);
            tx1.commit();
            session.close();
        }
    }

    public void update(Student student) {
        try(Session session = HibernateUtils.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();

            if (student.getGroupCode() != null) {
                List<Group> groups = session.createQuery(
                        "FROM Group WHERE group_code = "
                                + student.getGroupCode()).list();
                if (groups == null || groups.size() == 0) {
                    student.setGroupCode(null);
                }
            }
            session.update(student);
            tx1.commit();
            session.close();
        }
    }

    public void delete(Student student) {
        try (Session session = HibernateUtils.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();

            if (student.getGroupCode() != null) {

                List<Group> groups = session.createQuery(
                        "FROM Group WHERE group_code = "
                                + student.getGroupCode()).list();

                if (groups != null && groups.size() != 0) {

                    Group group = groups.get(0);

                    if (group.getStudentsNumber() > 0){
                        group.setStudentsNumber(group.getStudentsNumber() - 1);
                    }

                }
            }

            List<EntranceTest> tests = session.createQuery(
                    "FROM Group WHERE group_code = "
                            + student.getGroupCode()).list();

            if (tests != null && tests.size() != 0) {

                for (EntranceTest test : tests){
                    session.delete(test);
                }

            }

            session.delete(student);
            tx1.commit();
            session.close();
        }
    }

    public List<Student> findAll() {
        List<Student> students =
                (List<Student>)HibernateUtils.getSessionFactory()
                        .openSession().createQuery("From Student").list();
        return students;
    }
}
