package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

@SuppressWarnings("all")
public class UserDao {
    public User findById(long id) {
        return HibernateUtils.getSessionFactory().openSession().get(User.class, id);
    }

    public void save(User user) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(user);
        tx1.commit();
        session.close();
    }

    public void update(User user) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    public void delete(User user) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(user);
        tx1.commit();
        session.close();
    }

    public List<User> findAll() {
        List<User> users = (List<User>)  HibernateUtils.getSessionFactory().openSession().createQuery("From User").list();
        return users;
    }

    public User findByName(String name){
        try(Session session = HibernateUtils.getSessionFactory().openSession()){
            List<User> user = session.createQuery(
                    "FROM User WHERE name = "
                            + name).list();
            session.close();

            if (user == null || user.size() == 0){
                return null;
            }else {
                return user.get(0);
            }
        }
    }

    public <T> T query(String query){
        try (Session session = HibernateUtils.getSessionFactory().openSession()){
            return (T) session.createSQLQuery(query);
        }catch (Exception e){
            throw new RuntimeException("invalid sql query");
        }
    }
}

