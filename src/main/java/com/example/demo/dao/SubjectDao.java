package com.example.demo.dao;

import com.example.demo.HibernateUtils;
import com.example.demo.entities.Subject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

@SuppressWarnings("all")
public class SubjectDao {
    public Subject findBySubjectCode(long subjectCode) {
        return HibernateUtils.getSessionFactory().openSession().get(Subject.class, subjectCode);
    }

    public void save(Subject subject) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(subject);
        tx1.commit();
        session.close();
    }

    public void update(Subject subject) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(subject);
        tx1.commit();
        session.close();
    }

    public void delete(Subject subject) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(subject);
        tx1.commit();
        session.close();
    }

    public List<Subject> findAll() {
        List<Subject> subjects = (List<Subject>)  HibernateUtils.getSessionFactory().openSession().createQuery("From Subject").list();
        return subjects;
    }
}
